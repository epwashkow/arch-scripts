#!/bin/sh

# https://wiki.archlinux.org/title/Systemd-boot#systemd_service

sudo systemctl enable systemd-boot-update.service
echo "Please restart computer"
