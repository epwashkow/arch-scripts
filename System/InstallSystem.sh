#!/bin/sh

# dosfstools is required to create/modify FAT filesystems
# linux-firmware is required to support a wide range of hardware
# udisks2 is required for UEFI fwupd
sudo pacman -S \
    base \
    base-devel \
    bluez \
    cups \
    dosfstools \
    flatpak \
    fwupd \
    hplip \
    linux-firmware \
    nano \
    ntfs-3g \
    pacman-contrib \
    reflector \
    system-config-printer \
    udisks2

# Enable system services
sudo systemctl enable bluetooth.service
sudo systemctl enable cups.socket

# https://wiki.archlinux.org/title/reflector#systemd_timer
sudo systemctl enable reflector.timer
