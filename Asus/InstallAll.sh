#!/bin/sh

# Install core first
. $(dirname $0)/InstallCore.sh

# https://asus-linux.org/wiki/arch-guide/#rog-control-center
sudo pacman -S rog-control-center
