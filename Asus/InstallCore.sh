#!/bin/sh

# https://asus-linux.org/guides/arch-guide/
# 
# Note: official instructions say to install switcheroo-control, however, that library
# is only used by the Gnome desktop enviroment to switch between hybrid graphics cards.
# Since that library does not appear to be necessary for supergfxctl to operate,
# switcheroo-control is omitted from this script and is available on the Gnome install.

# Add mirror
sudo pacman-key --recv-keys 8F654886F17D497FEFE3DB448B15A6B0E9A3FA35
sudo pacman-key --finger 8F654886F17D497FEFE3DB448B15A6B0E9A3FA35
sudo pacman-key --lsign-key 8F654886F17D497FEFE3DB448B15A6B0E9A3FA35
sudo pacman-key --finger 8F654886F17D497FEFE3DB448B15A6B0E9A3FA35

echo "" | sudo tee -a /etc/pacman.conf
echo "[g14]" | sudo tee -a /etc/pacman.conf
echo "Server = https://arch.asus-linux.org" | sudo tee -a /etc/pacman.conf

sudo pacman -Syu

# Install packages
sudo pacman -S asusctl power-profiles-daemon supergfxctl
sudo systemctl enable power-profiles-daemon.service
sudo systemctl enable supergfxd
