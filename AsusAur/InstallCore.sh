#!/bin/sh

# https://asus-linux.org/guides/arch-guide/
# 
# Note: official instructions say to install switcheroo-control, however, that library
# is only used by the Gnome desktop enviroment to switch between hybrid graphics cards.
# Since that library does not appear to be necessary for supergfxctl to operate,
# switcheroo-control is omitted from this script and is available on the Gnome install.

yay -S asusctl power-profiles-daemon supergfxctl
sudo systemctl enable power-profiles-daemon.service
sudo systemctl enable supergfxd
