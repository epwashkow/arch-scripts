#!/bin/sh

# https://wiki.archlinux.org/title/PipeWire#Installation

# pipewire-alsa/-jack/-pulse depend on pipewire
sudo pacman -S \
    pipewire-alsa \
    pipewire-jack \
    pipewire-pulse
