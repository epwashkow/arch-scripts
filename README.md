# Arch Scripts

## Step 1: Install

1. Create installation medium
    * [USB instructions](https://wiki.archlinux.org/title/USB_flash_installation_medium)
1. Boot from installation medium
1. Connect to Internet
    * [WiFi instructions](https://wiki.archlinux.org/title/Iwd#iwctl)
    * To verify connection
        1. `ping archlinux.org`
        1. Wait for several tries
        1. `Ctrl+C` to stop
        1. Verify connection was successful
1. Run `archinstall`
    * Bootloader: choose "systemd-boot"
    * Profile: choose "Minimal"
    * Audio: choose "No audio server"
    * Network configuration: choose "NetworkManager"
    * Adjust the rest as you like
1. `chroot` into the new machine when prompted

## Step 2: After chroot

1. `pacman -S git`
1. `git clone https://gitlab.com/epwashkow/arch-scripts`
1. `cd arch-scripts`
1. `System/`
    * `InstallSystem.sh`
    * If using `systemd-boot`, run `EnableSystemdBootUpdateService.sh`
1. `Audio/InstallPipewire.sh`
    * If prompted, choose `wireplumber`
1. `Graphics/` _(select one or more)_
    * `InstallAmd.sh`
    * `InstallIntel.sh` _(>= 2014)_
    * `InstallNvidia.sh`
        * Hybrid graphics: `InstallNvidiaPrime.sh`
        * XOrg: `nvidia-xconfig`
        * Wayland: `pacman -S xorg-xwayland` _([source](https://wiki.archlinux.org/title/Wayland#Xwayland))_
1. `DesktopEnvironment/` _(select one)_
    * `Gnome/InstallGnome.sh`
    * `Plasma/InstallPlasma.sh`
        * if prompted, choose...
            * `qt6-multimedia-ffmpeg`
            * `tesseract-data-` for your language
                * English: `tesseract-data-eng`
1. Delete this repo
    1. `cd ..`
    1. `rm -rdf arch-scripts`
1. Poweroff
    1. `exit` _(closes `chroot`)_
    1. `poweroff`

## Step 3: Setup User

1. Connect to WiFi
1. Clone repo
    1. `mkdir` directory under home
    1. `cd` to that new directory
    1. `git clone https://gitlab.com/epwashkow/arch-scripts`
1. `cd arch-scripts`
1. `User/SetupUser.sh`

## Step 4: Setup System

If you...

* want Gnome + Fractional scaling + Variable Refresh Rate
    * `DesktopEnvironment/Gnome/EnableExperimentalFeatures.sh`
* want AUR: `AUR/InstallYayAur.sh`
    * _(Required for following `*Aur` scripts)_
* want ZSH
    1. `System/InstallOhMyZshAur.sh`
    1. When `/etc/default/useradd` is opened, change this line to `SHELL=/usr/bin/zsh`
* have an Asus laptop...
    1. Install drivers
        * `Asus` _(official)_: [-] requires 3rd party repo which can break, [+] quick install
            * `InstallCore.sh` -  Install Core
            * `InstallAll.sh` -  Install Core + ROG GUI
        * `AsusAur` _(unofficial)_: [+] no 3rd party repo to break, [-] long build time
            * `InstallCore.sh` -  Install Core
            * `InstallAll.sh` -  Install Core + ROG GUI
    1. Reboot
    1. Change settings: `sudo nano /etc/asusd/asusd.ron`
        * `charge_control_end_threshold`: battery % charge limit

## Step 5: Modify Boot

1. Modify boot parameters
    1. Open bootloader config
        * for `systemd-boot`
            1. `ls /boot/loader/entries/`
            1. Copy name of non-`fallback.conf` entry
            1. `sudo nano /boot/loader/entries/<entry.conf>` _(replace `<entry.conf>` with the name of the entry)_
    1. Make edits
        * Remove `nomodeset` or `vga=` _([Source](https://wiki.archlinux.org/title/AMDGPU#Loading))_
        * [Silent boot](https://wiki.archlinux.org/index.php/silent_boot#Kernel_parameters)
        * [Nvidia DRM](https://wiki.archlinux.org/title/NVIDIA#DRM_kernel_mode_setting)
1. Change timeout to zero
    * `systemd-boot`
        1. `sudo nano /boot/loader/loader.conf`
        1. Modify `timeout`
        1. Save and close
1. Rebuild bootloader
    * [`grub` instructions](https://wiki.archlinux.org/index.php/GRUB#Generated_grub.cfg)
    * `systemd-boot`: N/A
1. Modify `mkinitcpio`
    1. `sudo nano /etc/mkinitcpio.conf`
    1. Edit
        * `HOOKS`
            * Replace "udev usr resume" with "systemd" _(sources [1](https://wiki.archlinux.org/index.php/mkinitcpio#Common_hooks), [2](https://bbs.archlinux.org/viewtopic.php?id=169988))_
        * `MODULES`
            * [load graphics card early](https://wiki.archlinux.org/title/Kernel_mode_setting#Early_KMS_start) _(fixes most Desktop Environments)_
    1. Save
    1. `sudo mkinitcpio -P`
1. Reboot

## Step 6: Finalize Setup

1. Install applications _(see `App/` dir for specialized installs)_
    * [Firefox Gnome Theme](https://github.com/rafaelmardojai/firefox-gnome-theme): makes Firefox look like a native Gnome app
1. Update system
    1. `yay -Syu`
    1. `System/RemoveOrphanedPackages.sh`
    1. Resolve `*.pacnew` conflicts
        1. `sudo pacdiff -o`
            * _(A diffing tool can also be configured: see https://wiki.archlinux.org/title/Pacman/Pacnew_and_Pacsave#pacdiff)_
        1. Resolve conflicts
    1. `yay -Sc`
    1. [Update firmware](https://wiki.archlinux.org/title/fwupd#Usage)
1. Power off
