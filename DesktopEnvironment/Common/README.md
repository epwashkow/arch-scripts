# Common desktop packages

## Noto fonts

These are the best choice fonts for most desktop environments because they support international languages, are installed by KDE Plasma already, are recommended for Gnome, and just look good.

## xdg-user-dirs-gtk

This package will create common user directories if they are ever deleted. The GTK variant is preferred as it typically creates a couple more commonly-used directories.

## xdg-utils

Required by all desktop environments to open links in applications
