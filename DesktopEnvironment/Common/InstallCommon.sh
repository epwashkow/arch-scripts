#!/bin/sh

# Install common packages
sudo pacman -S \
    noto-fonts \
    noto-fonts-cjk \
    noto-fonts-emoji \
    xdg-user-dirs-gtk \
    xdg-utils
