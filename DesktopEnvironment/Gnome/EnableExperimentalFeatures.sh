#!/bin/sh

# https://github.com/GNOME/mutter/blob/main/data/org.gnome.mutter.gschema.xml.in
gsettings set org.gnome.mutter experimental-features '["scale-monitor-framebuffer", "variable-refresh-rate", "xwayland-native-scaling"]'
