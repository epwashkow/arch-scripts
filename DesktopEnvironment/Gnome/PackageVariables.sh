#!/bin/sh

# https://wiki.archlinux.org/title/GStreamer

# gnome-initial-setup depends on gdm and gnome-control-center
# totem depends on gst-plugins
packages="\
    file-roller \
    gnome-backgrounds \
    gnome-calculator \
    gnome-console \
    gnome-disk-utility \
    gnome-initial-setup \
    gnome-software \
    gnome-system-monitor \
    gnome-text-editor \
    gnome-tweaks \
    gnome-weather \
    gst-libav \
    gst-plugins-ugly \
    gvfs-afc \
    gvfs-dnssd \
    gvfs-goa \
    gvfs-google \
    gvfs-gphoto2 \
    gvfs-mtp \
    gvfs-nfs \
    gvfs-onedrive \
    gvfs-smb \
    gvfs-wsdd \
    loupe \
    nautilus \
    switcheroo-control \
    totem \
    xdg-desktop-portal-gnome"
