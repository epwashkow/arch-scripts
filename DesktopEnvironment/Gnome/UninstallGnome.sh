#!/bin/sh

# Get package variables
. $(dirname $0)/PackageVariables.sh

# Install packages
sudo pacman -Rns $packages
