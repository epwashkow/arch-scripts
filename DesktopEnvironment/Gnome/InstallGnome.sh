#!/bin/sh

# Install common desktop environment packages
$(dirname $0)/../Common/InstallCommon.sh

# Get package variables
. $(dirname $0)/PackageVariables.sh

# Install packages
sudo pacman -S $packages

# Enable services
sudo systemctl enable gdm
sudo systemctl enable switcheroo-control
