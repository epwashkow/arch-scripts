#!/bin/sh

# Variables
thisDir=$(dirname $0)
. $thisDir/Variables.sh

# Install common desktop environment packages
$thisDir/../Common/InstallCommon.sh

# Install packages
sudo pacman -S $packages

# Enable Wayland on SDDM
# https://wiki.archlinux.org/title/SDDM#Wayland
sudo mkdir -p $sddmConfDir
sudo cp $thisDir/10-wayland.conf $sddmConfDir/10-wayland.conf

# Enable services
sudo systemctl enable sddm.service
