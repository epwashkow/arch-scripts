#!/bin/sh

# Variables
. $(dirname $0)/Variables.sh

# Uninstall packages
sudo pacman -Rns $packages
sudo systemctl disable sddm
sudo rm -rf $sddmConfDir
