#!/bin/sh

# https://wiki.archlinux.org/title/NVIDIA
# https://wiki.archlinux.org/title/xorg#Driver_installation

# nvidia-settings depends on nvidia-utils
sudo pacman -S nvidia nvidia-settings
