#!/bin/sh

# https://wiki.archlinux.org/title/AMDGPU#Installation

sudo pacman -S \
    mesa \
    vulkan-radeon
